@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
    <ul>
        <label for="nama"> Nama Lengkap : </label> <br><br>
        <input type="text" name="nama" id="nama"> <br><br>
    </ul>
    <ul>
        <label for="address"> Alamat : </label> <br><br>
        <textarea name="address" id="address" cols="30" rows="10"></textarea> <br><br>
    </ul>
    <ul>
        <input type="submit" value="kirim">
    </ul>
</form>
@endsection